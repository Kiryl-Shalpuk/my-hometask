package fourthLessonHomeTask;

public class Task2 {
    // Передать числа и уточнить четные они или нет
    public static void main(String[] args) {
        int array[] = new int[15];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * 75);
            EvenNumb(array[i]);
        }
    }

    public static void EvenNumb(int numb){
        if (numb % 2 == 0 && numb != 0){
            System.out.println("Число " + numb + " - " + true);
        }
        else{
            System.out.println("Число " + numb + " - " + false);
        }
    }
}
