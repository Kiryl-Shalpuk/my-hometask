package fourthLessonHomeTask;

public class Task4 {
    // Найти куб числа
    public static void main(String[] args) {
        int array[] = new int[10];
        for (int i = 1; i < array.length; i++){
            array[i] = i;
            int r = cub(array[i]);
            System.out.println(array[i] + " в кубе: " + r);
        }
    }

    public static int cub(int numb){
        return numb * numb * numb;
    }
}
